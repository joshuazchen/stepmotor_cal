// ModCal 为程序逻辑模块, 使逻辑与界面分离

#include "stdafx.h"
#include "ModCal.h"
#include "resource.h"
#include "StepMotorCal.h"

static int RadioCalBase;       // 加速曲线计算方式
static int CheckKeepLastData;  // 保留上次数据
static int EditClockFreq;      // 系统时钟频率
static int EditTimerPrescale;  // 定时器预分频值
static int EditBufSteps;       // 变速阶段步数
static int ComboNumPerLine;    // 每行输出元素个数
static int EditStepsPerRound;  // 电机转一圈所需步数

static double EditStartSpeed;  // 起始转速
static double EditEndSpeed;    // 最终转速
static double EditStartFreq;   // 起始脉冲频率
static double EditEndFreq;     // 最终脉冲频率

// 程序标题
static char AppTitle[99];
static CString EditCurveTable; // 加速曲线表内容
static int ReadmeShowing;      // 正在显示程序说明
extern char *Readme;           // 程序说明(在程序最后)

// 从界面获取数据
void GetData(void)
{
	char str[256];

	// 加速曲线计算方式
	if (dlg.IsDlgButtonChecked(IDC_RADIO_BASE_ON_SM_SPEED)) {
		RadioCalBase = 1;
	} else if (dlg.IsDlgButtonChecked(IDC_RADIO_BASE_ON_PULSE_FREQ)) {
		RadioCalBase = 2;
	}
	
	// 是否保留上次数据
	CheckKeepLastData = dlg.IsDlgButtonChecked(IDC_CHECK_KEEP_LAST_DATA);

	// 系统时钟频率
	memset(str, 0, sizeof(str));
	dlg.GetDlgItemText(IDC_EDIT_CLOCK_FREQ, str, sizeof(str));
	EditClockFreq = atoi(str);

	// 定时器预分频值
	memset(str, 0, sizeof(str));
	dlg.GetDlgItemText(IDC_EDIT_TIMER_PRESCALE, str, sizeof(str));
	EditTimerPrescale = atoi(str);

	// 电机转一圈所需步数
	memset(str, 0, sizeof(str));
	dlg.GetDlgItemText(IDC_EDIT_STEPS_PER_ROUND, str, sizeof(str));
	EditStepsPerRound = atoi(str);

	// 变速阶段步数
	memset(str, 0, sizeof(str));
	dlg.GetDlgItemText(IDC_EDIT_BUF_STEPS, str, sizeof(str));
	EditBufSteps = atoi(str);

	// 每行输出元素个数
	ComboNumPerLine = ((CComboBox *)dlg.GetDlgItem(IDC_COMBO_NUM_PER_LINE))->GetCurSel();

	// 起始转速
	memset(str, 0, sizeof(str));
	dlg.GetDlgItemText(IDC_EDIT_START_SPEED, str, sizeof(str));
	EditStartSpeed = atof(str);

	// 最终转速
	memset(str, 0, sizeof(str));
	dlg.GetDlgItemText(IDC_EDIT_END_SPEED, str, sizeof(str));
	EditEndSpeed = atof(str);

	// 起始脉冲频率
	memset(str, 0, sizeof(str));
	dlg.GetDlgItemText(IDC_EDIT_START_FREQ, str, sizeof(str));
	EditStartFreq = atof(str);

	// 最终脉冲频率
	memset(str, 0, sizeof(str));
	dlg.GetDlgItemText(IDC_EDIT_END_FREQ, str, sizeof(str));
	EditEndFreq = atof(str);

	// 加速曲线表内容
	dlg.GetDlgItemText(IDC_EDIT_CURVE_TABLE, EditCurveTable);
}

// 将数据显示到界面
void SetData(void)
{
	char str[256];

	// 系统时钟频率
	memset(str, 0, sizeof(str));
	sprintf(str, "%d", EditClockFreq);
	dlg.SetDlgItemText(IDC_EDIT_CLOCK_FREQ, str);

	// 定时器预分频值
	memset(str, 0, sizeof(str));
	sprintf(str, "%d", EditTimerPrescale);
	dlg.SetDlgItemText(IDC_EDIT_TIMER_PRESCALE, str);

	// 电机转一圈所需步数
	memset(str, 0, sizeof(str));
	sprintf(str, "%d", EditStepsPerRound);
	dlg.SetDlgItemText(IDC_EDIT_STEPS_PER_ROUND, str);

	// 变速阶段步数
	memset(str, 0, sizeof(str));
	sprintf(str, "%d", EditBufSteps);
	dlg.SetDlgItemText(IDC_EDIT_BUF_STEPS, str);

	// 起始转速
	memset(str, 0, sizeof(str));
	sprintf(str, "%.2f", EditStartSpeed);
	dlg.SetDlgItemText(IDC_EDIT_START_SPEED, str);

	// 最终转速
	memset(str, 0, sizeof(str));
	sprintf(str, "%.2f", EditEndSpeed);
	dlg.SetDlgItemText(IDC_EDIT_END_SPEED, str);

	// 起始脉冲频率
	memset(str, 0, sizeof(str));
	sprintf(str, "%.2f", EditStartFreq);
	dlg.SetDlgItemText(IDC_EDIT_START_FREQ, str);

	// 最终脉冲频率
	memset(str, 0, sizeof(str));
	sprintf(str, "%.2f", EditEndFreq);
	dlg.SetDlgItemText(IDC_EDIT_END_FREQ, str);

	// 显示加速曲线表内容
	((CEdit *)dlg.GetDlgItem(IDC_EDIT_CURVE_TABLE))->SetRedraw(FALSE);
	dlg.SetDlgItemText(IDC_EDIT_CURVE_TABLE, EditCurveTable);
	// 光标移至最后
	((CEdit *)dlg.GetDlgItem(IDC_EDIT_CURVE_TABLE))->SetSel(-1);
	// 滚动至最后一行
	int TableLine = ((CEdit *)dlg.GetDlgItem(IDC_EDIT_CURVE_TABLE))->GetLineCount();
	((CEdit *)dlg.GetDlgItem(IDC_EDIT_CURVE_TABLE))->LineScroll(TableLine);
	((CEdit *)dlg.GetDlgItem(IDC_EDIT_CURVE_TABLE))->SetRedraw(TRUE);
	//dlg.GetDlgItem(IDC_EDIT_CURVE_TABLE)->SetFocus();
}

// 初始化控件按钮
void InitWidget(void)
{
	// 设置对话框标题
	memset(AppTitle, 0, sizeof(AppTitle));
	strncpy(AppTitle, APP_NAME, sizeof(AppTitle)-1);
	strncat(AppTitle, APP_VER, sizeof(AppTitle)-strlen(AppTitle)-1);
	dlg.SetWindowText(AppTitle);

	// 初始化RadioBox
	((CButton *)dlg.GetDlgItem(IDC_RADIO_BASE_ON_SM_SPEED))->SetCheck(TRUE);
	// 初始化CheckBox
	((CButton *)dlg.GetDlgItem(IDC_CHECK_KEEP_LAST_DATA))->SetCheck(FALSE);
	// 初始化ComboBox
	((CComboBox *)dlg.GetDlgItem(IDC_COMBO_NUM_PER_LINE))->InsertString(0, "1");
	((CComboBox *)dlg.GetDlgItem(IDC_COMBO_NUM_PER_LINE))->InsertString(1, "2");
	((CComboBox *)dlg.GetDlgItem(IDC_COMBO_NUM_PER_LINE))->InsertString(2, "3");
	((CComboBox *)dlg.GetDlgItem(IDC_COMBO_NUM_PER_LINE))->InsertString(3, "4");
	((CComboBox *)dlg.GetDlgItem(IDC_COMBO_NUM_PER_LINE))->InsertString(4, "5");
	((CComboBox *)dlg.GetDlgItem(IDC_COMBO_NUM_PER_LINE))->InsertString(5, "6");
	((CComboBox *)dlg.GetDlgItem(IDC_COMBO_NUM_PER_LINE))->InsertString(6, "7");
	((CComboBox *)dlg.GetDlgItem(IDC_COMBO_NUM_PER_LINE))->InsertString(7, "8");
	((CComboBox *)dlg.GetDlgItem(IDC_COMBO_NUM_PER_LINE))->InsertString(8, "9");
	((CComboBox *)dlg.GetDlgItem(IDC_COMBO_NUM_PER_LINE))->InsertString(9, "10");
	((CComboBox *)dlg.GetDlgItem(IDC_COMBO_NUM_PER_LINE))->InsertString(10, "Max");
	((CComboBox *)dlg.GetDlgItem(IDC_COMBO_NUM_PER_LINE))->SetCurSel(4);
}

// 数据初始化
void InitData(void)
{
	// 初始化变量
	EditClockFreq = 0;
	EditTimerPrescale = 0;
	EditStepsPerRound = 0;
	EditStartSpeed = 0;
	EditEndSpeed = 0;
	EditBufSteps = 0;
	EditStartFreq = 0;
	EditEndFreq = 0;
}

// 切换至基于电机转速来计算
void BaseOnSmSpeed(void)
{
	dlg.GetDlgItem(IDC_EDIT_STEPS_PER_ROUND)->EnableWindow(TRUE);
	dlg.GetDlgItem(IDC_EDIT_START_SPEED)->EnableWindow(TRUE);
	dlg.GetDlgItem(IDC_EDIT_END_SPEED)->EnableWindow(TRUE);

	dlg.GetDlgItem(IDC_EDIT_START_FREQ)->EnableWindow(FALSE);
	dlg.GetDlgItem(IDC_EDIT_END_FREQ)->EnableWindow(FALSE);

	EditStartFreq = 0;
	EditEndFreq = 0;
}

// 切换至基于脉冲频率来计算
void BaseOnPulseFreq(void)
{
	dlg.GetDlgItem(IDC_EDIT_START_FREQ)->EnableWindow(TRUE);
	dlg.GetDlgItem(IDC_EDIT_END_FREQ)->EnableWindow(TRUE);

	dlg.GetDlgItem(IDC_EDIT_STEPS_PER_ROUND)->EnableWindow(FALSE);
	dlg.GetDlgItem(IDC_EDIT_START_SPEED)->EnableWindow(FALSE);
	dlg.GetDlgItem(IDC_EDIT_END_SPEED)->EnableWindow(FALSE);

	EditStepsPerRound = 0;
	EditStartSpeed = 0;
	EditEndSpeed = 0;
}

// 使用默认值
void SetDefault(void)
{
	EditClockFreq = FOSC;
	EditTimerPrescale = TIMER_PRESCALE;
	EditBufSteps = MAX_BUFF_STEPS;

	if (RadioCalBase == 1) {
		EditStepsPerRound = STEPS_PER_ROUND;
		EditStartSpeed = START_SPEED;
		EditEndSpeed = END_SPEED;
	} else if (RadioCalBase == 2) {
		EditStartFreq = START_FREQ;
		EditEndFreq = END_FREQ;
	}
}

// 清空参数
void ClearArgs(void)
{
	EditClockFreq = 0;
	EditTimerPrescale = 0;
	EditStepsPerRound = 0;
	EditStartSpeed = 0;
	EditEndSpeed = 0;
	EditBufSteps = 0;
	EditStartFreq = 0;
	EditEndFreq = 0;
}

// 清空曲线表
void ClearTable(void)
{
	EditCurveTable = "";
}

// 加速曲线计算
void CurveCal(void)
{
	int count;          // 每行的数据计数
	int NumPerLine;     // 每行显示最大数据个数
	int comp_value;     // 定时器比较值
	double FreqPerStep; // 每走一步脉冲变化值
	char buf[1024];

	// 数据有效性检查
	if (EditClockFreq < 1 || EditTimerPrescale < 1 || EditBufSteps < 2
		|| (RadioCalBase == 1
			&& (EditStepsPerRound < 6 || EditStartSpeed < 1 || EditEndSpeed < 1))
		|| (RadioCalBase == 2
			&& (EditStartFreq < 1 || EditEndFreq < 1))) {
		dlg.MessageBox("请输入有效参数", "提示");
		return;
	}

	// 检查是否需清空上次数据
	if (!CheckKeepLastData || ReadmeShowing) {
		ReadmeShowing = 0;
		ClearTable();
	}

	EditCurveTable += "\r\n===========================================================\r\n";
	if (RadioCalBase == 1) {
		sprintf(buf, "电机起始转速: %.2f r/min, 最终转速: %.2f r/min", EditStartSpeed, EditEndSpeed);
		EditCurveTable += buf;
		// 根据电机转速计算脉冲频率
		EditStartFreq = EditStepsPerRound * EditStartSpeed / 60;
		EditEndFreq = EditStepsPerRound * EditEndSpeed / 60;
	} else if (RadioCalBase == 2) {
		sprintf(buf, "起始脉冲频率: %.2f Hz, 最大频率: %.2f Hz", EditStartFreq, EditEndFreq);
		EditCurveTable += buf;
	}
	EditCurveTable += "\r\n-----------------------------------------------------------\r\n";

	FreqPerStep = (EditEndFreq - EditStartFreq) / (EditBufSteps - 1);

	count = 0;
	// 设置每行数据个数
	if (ComboNumPerLine >= 10) {
		NumPerLine = 0xFFFF; // 设为最大(即全部在一行显示)
	} else {
		NumPerLine = ComboNumPerLine + 1;
	}
	for (int i = 0; i < EditBufSteps; i++) {
		// 定时器比较值(定时器频率为脉冲频率的2倍)
		comp_value = (int)(EditClockFreq / EditTimerPrescale / ((EditStartFreq + i * FreqPerStep) * 2) - 1);
		sprintf(buf, "\t0x%04X,", comp_value);
		// 检查数据是否需换行显示
		if (count == NumPerLine) {
			count = 0;
			EditCurveTable += "\r\n";
		}
		EditCurveTable += buf;
		count++;
	}
	EditCurveTable += "\r\n\r\n";
}

// 显示程序说明
void ShowReadme(void)
{
	ReadmeShowing = 1;
	EditCurveTable = Readme;
}

// 程序说明
static char *Readme = "\
\r\n\
    说明:\r\n\
\r\n\
    通过不断改变定时器的输出比较值, 使驱动电机的脉冲频率\r\n\
产生渐变, 从而达到电机速度的平滑变化.\r\n\
\r\n\
    本程序可生成使脉冲频率线性变化的定时器输出比较值.";
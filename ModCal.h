// ModCal 为程序逻辑模块, 使逻辑与界面分离

#ifndef __MODCAL_H
#define __MODCAL_H

// 程序名称与版本号
#define APP_NAME "电机加速曲线生成工具 "
#define APP_VER  "v1.19"

#define FOSC 16000000L       // 系统时钟
#define TIMER_PRESCALE 1     // 控制步进电机的定时器预分频值
#define STEPS_PER_ROUND 1600 // 电机转一圈所需步数
#define START_SPEED 10       // 最小速度, 单位(r/min)
#define END_SPEED 150        // 最大速度, 单位(r/min)
#define MAX_BUFF_STEPS 100   // 变速阶段最大持续步数

// 起始脉冲频率, 此时电机处于最小速度
#define START_FREQ (STEPS_PER_ROUND * START_SPEED / 60)
// 最终脉冲频率, 此时电机达到最大速度
#define END_FREQ (STEPS_PER_ROUND * END_SPEED / 60)

// 从界面获取数据
void GetData(void);
// 将数据显示到界面
void SetData(void);
// 初始化控件按钮
void InitWidget(void);
// 数据初始化
void InitData(void);
// 切换至基于电机转速来计算
void BaseOnSmSpeed(void);
// 切换至基于脉冲频率来计算
void BaseOnPulseFreq(void);
// 使用默认值
void SetDefault(void);
// 清空参数
void ClearArgs(void);
// 清空曲线表
void ClearTable(void);
// 加速曲线计算
void CurveCal(void);
// 显示程序说明
void ShowReadme(void);


#endif
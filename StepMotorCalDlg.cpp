// 步进电机加速曲线生成工具

#include <stdio.h>
#include "stdafx.h"
#include "StepMotorCal.h"
#include "StepMotorCalDlg.h"
#include "ModCal.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CStepMotorCalDlg 对话框
CStepMotorCalDlg::CStepMotorCalDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CStepMotorCalDlg::IDD, pParent)
{
//	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CStepMotorCalDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CStepMotorCalDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_CLEAR_ARGS, &CStepMotorCalDlg::OnBnClickedButtonClearArgs)
	ON_BN_CLICKED(IDC_BUTTON_SET_DEFAULT, &CStepMotorCalDlg::OnBnClickedButtonSetDefault)
	ON_BN_CLICKED(IDC_RADIO_BASE_ON_SM_SPEED, &CStepMotorCalDlg::OnBnClickedRadioBaseOnSmSpeed)
	ON_BN_CLICKED(IDC_RADIO_BASE_ON_PULSE_FREQ, &CStepMotorCalDlg::OnBnClickedRadioBaseOnPulseFreq)
	ON_BN_CLICKED(IDC_BUTTON_CURVE_CAL, &CStepMotorCalDlg::OnBnClickedButtonCurveCal)
	ON_BN_CLICKED(IDC_BUTTON_CLEAR_TABLE, &CStepMotorCalDlg::OnBnClickedButtonClearTable)
	ON_BN_CLICKED(IDC_BUTTON_README, &CStepMotorCalDlg::OnBnClickedButtonReadme)
END_MESSAGE_MAP()


// CStepMotorCalDlg 消息处理程序

BOOL CStepMotorCalDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// 初始化控件按钮
	InitWidget();
	// 数据初始化
	InitData();
	// 选择根据电机速度计算加速曲线
	OnBnClickedRadioBaseOnSmSpeed();
	// 根据计算方式设置默认参数
	OnBnClickedButtonSetDefault();

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CStepMotorCalDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标显示。
//
HCURSOR CStepMotorCalDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

// 清空参数
void CStepMotorCalDlg::OnBnClickedButtonClearArgs()
{
	GetData();
	ClearArgs();
	SetData();
}

// 根据计算方式设置默认参数
void CStepMotorCalDlg::OnBnClickedButtonSetDefault()
{
	GetData();
	SetDefault();
	SetData();
}

// 选择根据电机速度计算加速曲线
void CStepMotorCalDlg::OnBnClickedRadioBaseOnSmSpeed()
{
	GetData();
	BaseOnSmSpeed();
	SetData();
}

// 选择根据脉冲频率计算加速曲线
void CStepMotorCalDlg::OnBnClickedRadioBaseOnPulseFreq()
{
	GetData();
	BaseOnPulseFreq();
	SetData();
}

// 步进电机加速曲线计算
void CStepMotorCalDlg::OnBnClickedButtonCurveCal()
{
	GetData();
	CurveCal();
	SetData();
}

// 清空加速曲线表
void CStepMotorCalDlg::OnBnClickedButtonClearTable()
{
	GetData();
	ClearTable();
	SetData();
}

// 显示程序说明
void CStepMotorCalDlg::OnBnClickedButtonReadme()
{
	GetData();
	ShowReadme();
	SetData();
}

//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by StepMotorCal.rc
//
#define IDD_STEP_MOTOR_CAL_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDC_EDIT_CLOCK_FREQ             1000
#define IDC_EDIT_TIMER_PRESCALE         1001
#define IDC_EDIT_BUF_STEPS              1002
#define IDC_RADIO_BASE_ON_SM_SPEED      1003
#define IDC_RADIO_BASE_ON_PULSE_FREQ    1004
#define IDC_EDIT_STEPS_PER_ROUND        1006
#define IDC_EDIT_START_SPEED            1007
#define IDC_EDIT_END_SPEED              1008
#define IDC_EDIT_START_FREQ             1009
#define IDC_EDIT_END_FREQ               1010
#define IDC_BUTTON_CURVE_CAL            1011
#define IDC_EDIT_CURVE_TABLE            1012
#define IDC_BUTTON_SET_DEFAULT          1013
#define IDC_BUTTON_CLEAR_ARGS           1014
#define IDC_BUTTON_CLEAR_TABLE          1015
#define IDC_COMBO_NUM_PER_LINE          1016
#define IDC_BUTTON_README               1018
#define IDC_CHECK_KEEP_LAST_DATA        1019

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1020
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
